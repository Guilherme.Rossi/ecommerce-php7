var FormEditable = function () {

		function respostaBD(response){
				if(!response.success) {
						//return 'response.msg';
						toastr.success('O registro foi alterado com sucesso!', 'Atualização Cadastral');
				}else{
						toastr.error('Houve um problema e o registro não pode ser alterado!', 'Atualização Cadastral');
				};

				if(response.status == 'error') return response.msg; //msg will be shown in editable form
		}

		var initEditables = function () {

				//global settings
				$.fn.editable.defaults.mode = 'inline';
				$.fn.editable.defaults.inputclass = 'form-control';
				$.fn.editable.defaults.url = 'assets/inc/form-editable-agente.php';

				//editables element samples
				$('.nomeAgente').editable({
						type: 'text',
						name: 'nomeAgente',
						title: 'Enter username',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.cpf').editable({
						type: 'text',
						name: 'cpf',
						title: 'CPF',
						validate: function (value) {
							if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.rg').editable({
						type: 'text',
						name: 'rg',
						title: 'RG',
						emptytext: "Vazio",
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.tipoAgente').editable({
						showbuttons: true,
						type: "select",
						name: "tipoAgente",
						prepend: "Não selecionado",
						inputclass: 'form-control',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 3,
										text: 'Agente CLT'
								}, {
										value: 5,
										text: 'Sócio'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.email').editable({
						type: 'text',
						name: 'email',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
								if (validateEmail(value) == false) return '* Email inválido.';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.telefonecomercial').editable({
						type: 'text',
						name: 'telefonecomercial',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.telefonecelular').editable({
						type: 'text',
						name: 'telefonecelular',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.statusLink').editable({
						showbuttons: true,
						type: "select",
						name: "statusLink",
						inputclass: 'form-control',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 0,
										text: 'Solicitado'
								}, {
										value: 1,
										text: 'Enviado'
								}, {
										value: 2,
										text: 'Pendência Financeira'
								}, {
										value: 3,
										text: 'Concluído'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataStatusLink').editable({
						type: 'date',
						name: 'dataStatusLink',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.statusCtps').editable({
						showbuttons: true,
						type: "select",
						name: "statusCtps",
						inputclass: 'form-control',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 1,
										text: 'Recebido'
								}, {
										value: 2,
										text: 'Registrado'
								}, {
										value: 3,
										text: 'Devolvido'
								}, {
										value: 4,
										text: 'Desligado'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataStatusCtps').editable({
						type: 'date',
						name: 'dataStatusCtps',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.obsCtps').editable({
						showbuttons: 'bottom',
						type: 'textarea',
						name: 'obsCtps',
						emptytext: "Vazio",
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				var ufContrato = [];
				$.each({
					"RJ": "Rio de Janeiro",
					"SP": "São Paulo"
				}, function (k, v) {
					ufContrato.push({
						id: k,
						text: v
					});
				});

				$('.ufContrato').editable({
					showbuttons: true,
					type: "select",
					name: "ufContrato",
					inputclass: 'form-control',
					source: [
						{
							value: "RJ",
							text: "Rio de Janeiro"
						},
						{
							value: "SP",
							text: "São Paulo"
						}
					],
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				$('.tipoContrato').editable({
					showbuttons: 'bottom',
					type: 'select',
					name: 'tipoContrato',
					emptytext: "Vazio",
					source: [{
						value: 'HORISTA',
						text: 'HORISTA'
					}, {
						value: 'MENSALISTA',
						text: 'MENSALISTA'
					}
					],
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				$('.horasContrato').editable({
					showbuttons: 'bottom',
					type: 'select',
					name: 'horasContrato',
					emptytext: "Vazio",
					source: [{
						value: 1,
						text: '1'
					}, 
					{
						value: 2,
						text: '2'
					},
					{
						value: 3,
						text: '3'
					},
					{
						value: 4,
						text: '4'
					},
					{
						value: 5,
						text: '5'
					},
					{
						value: 6,
						text: '6'
					},
					{
						value: 7,
						text: '7'
					},
					{
						value: 8,
						text: '8'
					}
					],
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				$('.statusDoc').editable({
					showbuttons: true,
					type: "select",
					name: "statusDoc",
					inputclass: 'form-control',
					validate: function (value) {
							if ($.trim(value) == '') return 'Este campo é obrigatório';
					},
					source: [{
									value: 1,
									text: 'Recebido'
							}, {
									value: 0,
									text: 'Pendente'
							}
					],
					success: function(response, newValue) {
							respostaBD(response);
					}
				});

				$('.dataStatusDoc').editable({
						type: 'date',
						name: 'dataStatusDoc',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.obsDoc').editable({
						showbuttons: 'bottom',
						type: 'textarea',
						name: 'obsDoc',
						emptytext: "Vazio",
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.presencial').editable({
						showbuttons: true,
						type: "select",
						name: "presencial",
						inputclass: 'form-control',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 1,
										text: 'Presencial'
								}, {
										value: 0,
										text: 'Remoto'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataStatusTreinamento').editable({
						type: 'date',
						name: 'dataStatusTreinamento',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.numVoucher').editable({
						type: 'text',
						name: 'numVoucher',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.formaEnvio').editable({
						showbuttons: true,
						type: "select",
						name: "formaEnvio",
						inputclass: 'form-control',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 'PORTADOR',
										text: 'PORTADOR'
								}, {
										value: 'CORREIOS',
										text: 'CORREIOS'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataStatusMidiaTreinamento').editable({
						type: 'date',
						name: 'dataStatusMidiaTreinamento',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.envioTermos').editable({
						type: 'date',
						name: 'envioTermos',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.recTermos').editable({
						type: 'date',
						name: 'recTermos',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.obsTermos').editable({
						showbuttons: 'bottom',
						type: 'textarea',
						name: 'obsTermos',
						emptytext: "Vazio",
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.statusAgente').editable({
						showbuttons: true,
						type: "select",
						name: "statusAgente",
						inputclass: 'form-control',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 1,
										text: 'Habilitado'
								}, {
										value: 2,
										text: 'Desligado'
								}, {
										value: 3,
										text: 'Pendente'
								}, {
										value: 4,
										text: 'Restrição Financeira'
								}, {
										value: 5,
										text: 'Solicitado'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataStatusHabilitacao').editable({
						type: 'date',
						name: 'dataStatusHabilitacao',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.obsHabilitacao').editable({
						showbuttons: 'bottom',
						type: 'textarea',
						name: 'obsCtps',
						emptytext: "Vazio",
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.banco').editable({
						showbuttons: true,
						type: "select",
						name: "banco",
						title: 'Banco',
						emptytext: "Vazio",
						inputclass: 'form-control',
						source: [{
								value: '001',
								text: 'BANCO DO BRASIL'
							},{
								value: '341',
								text: 'ITAÚ'
							},{
								value: '033',
								text: 'SANTANDER'
							},{
								value: '237',
								text: 'BRADESCO'
							},{
								value: '745',
								text: 'CITIBANK'
							},{
								value: '104',
								text: 'CAIXA ECONÔMICA FEDERAL'
							},{
								value: '389',
								text: 'MERCANTIL DO BRASIL'
							},{
								value: '453',
								text: 'BANCO RURAL'
							},{
								value: '422',
								text: 'SAFRA'
							},{
								value: '399',
								text: 'HSBC BANK'
							},{
								value: '748',
								text: 'SICREDI'
							},{
								value: '756',
								text: 'SICOOB'
							}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				
				$('.agencia').editable({
						type: 'text',
						name: 'agencia',
						title: 'Agência',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				
				$('.numConta').editable({
						type: 'text',
						name: 'numConta',
						title: 'Núm. Conta',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				
				$('.operacao').editable({
						type: 'text',
						name: 'operacao',
						title: 'Operação',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				
				$('.titular').editable({
						type: 'text',
						name: 'titular',
						title: 'Nome do Titular',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				
				$('.docTitular').editable({
						type: 'text',
						name: 'docTitular',
						title: 'Documento do Titular',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				var statusITI = [];
				$.each({
					"Solicitado": "Solicitado",
					"Enviado": "Enviado",
					'Finalizado': "Finalizado"
				}, function (k, v) {
					statusITI.push({
						id: k,
						text: v
					});
				});

				$('.statusITI').editable({
					showbuttons: true,
					type: "select",
					name: "statusITI",
					inputclass: 'form-control',
					source: [
						{
							value: "Solicitado",
							text: "Solicitado"
						},
						{
							value: "Enviado",
							text: "Enviado"
						},
						{
							value: "Finalizado",
							text: "Finalizado"
						}
					],
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

			$('.dataCar').editable({
				type: 'date',
				name: 'dataCar',
				format: 'yyyy-mm-dd',
				viewformat: 'dd/mm/yyyy',
				datepicker: {
					rtl: Metronic.isRTL(),
					orientation: 'left',
					autoclose: true
				},
				success: function (response, newValue) {
					respostaBD(response);
				}
			});

			$('.certNumero').editable({
				type: 'text',
				name: 'certNumero',
				title: 'Certificado',
				emptytext: "Vazio",
				success: function (response, newValue) {
					respostaBD(response);
				}
			});

			$('.certEmissao').editable({
				type: 'date',
				name: 'dataStatusLink',
				format: 'yyyy-mm-dd',
				viewformat: 'dd/mm/yyyy',
				datepicker: {
					rtl: Metronic.isRTL(),
					orientation: 'left',
					autoclose: true
				},
				success: function (response, newValue) {
					respostaBD(response);
				}
			});

			$('.certVencimento').editable({
				type: 'date',
				name: 'dataStatusLink',
				format: 'yyyy-mm-dd',
				viewformat: 'dd/mm/yyyy',
				datepicker: {
					rtl: Metronic.isRTL(),
					orientation: 'left',
					autoclose: true
				},
				success: function (response, newValue) {
					respostaBD(response);
				}
			});

		}
		return {
				//main function to initiate the module
				init: function () {
						// init editable elements
						initEditables();
				}
		};
}();

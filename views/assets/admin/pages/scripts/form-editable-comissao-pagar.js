var FormEditableContaPagar = function () {

		function respostaBD(response){
				if(response == 'ok') {
						toastr.success('O registro foi alterado com sucesso!', 'Atualização Cadastral');
				}else{
						toastr.error('Houve um problema e o registro não pode ser alterado!', 'Atualização Cadastral');
				};

				if(response.status == 'error') return response.msg; //msg will be shown in editable form
		}

		var initEditables = function () {

				//global settings
				$.fn.editable.defaults.mode = 'inline';
				$.fn.editable.defaults.inputclass = 'form-control';
				$.fn.editable.defaults.url = 'assets/inc/form-editable-comissao-pagar.php';

				//editables element samples
				$('.vlrDescAgente').editable({
						type: 'text',
						name: 'vlrDescAgente',
						title: 'Valor de desconto Agente',
						emptytext: "0.00",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				//editables element samples
				$('.vlrDescTed').editable({
						type: 'text',
						name: 'vlrDescTed',
						title: 'Valor de desconto TED',
						emptytext: "0.00",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				//editables element samples
				$('.vlrSaldoAnterior').editable({
						type: 'text',
						name: 'vlrSaldoAnterior',
						title: 'Valor do Saldo Anterior',
						emptytext: "0.00",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				//editables element samples
				$('.vlrImposto').editable({
						type: 'text',
						name: 'vlrImposto',
						title: 'Valor do Imposto',
						emptytext: "0.00",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataNF').editable({
						type: 'date',
						name: 'dataNF',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						emptytext: "Vazio",
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataPagamento').editable({
						type: 'date',
						name: 'dataPagamento',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						emptytext: "Vazio",
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.obsContaPagar').editable({
						showbuttons: 'bottom',
						type: 'textarea',
						name: 'obsContaPagar',
						emptytext: "Vazio",
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

		}
		return {
				//main function to initiate the module
				init: function () {
						// init editable elements
						initEditables();
				}
		};
}();

var ChartsAmcharts = function() {

	var initChartEmissao = function () {
		var chart = AmCharts.makeChart("chartMesAtualAnterior", {
			"theme": "light",
			"type": "serial",
			"legend": {
				"horizontalGap": 10,
				"maxColumns": 1,
				"position": "bottom",
				"useGraphSettings": true,
				"markerSize": 10,
				"marginTop": 20
			},
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-emissao-media.php?mesAno=" + localStorage.getItem('dados') + "&tipo=emissaoMes"
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-emissao-media.php?mesAno=" + localStorage.getItem('dados') + "&tipo=emissaoMes"
			},
			"valueAxes": [{
				"stackType": "regular",
				"position": "left",
				"title": "Comparativo de emissões de certificados",
			}],
			"startDuration": 1,
			"graphs": [{
				"balloonText": "Certificados emitidos no dia [[category]] do mês atual: <b>[[value]]</b>",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"title": "Mês Atual",
				"type": "column",
				"valueField": "atual"
			}, {
				"balloonText": "Certificados emitidos no dia [[category]] do mês anterior: <b>[[value]]</b>",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"title": "Mês Anterior",
				"type": "column",
				"valueField": "anterior"
			}],
			"plotAreaFillAlphas": 0.1,
			"categoryField": "dia",
			"categoryAxis": {
				"gridPosition": "start"
			},
			"export": {
				"enabled": true
			}
		});

		$('#chartMesAtualAnterior').closest('.portlet').find('.fullscreen').click(function () {
			chart.invalidateSize();
		});
	};

	var initChartEmissaoSig = function () {
		var chart = AmCharts.makeChart("chartMesAtualAnteriorSig", {
			"theme": "light",
			"type": "serial",
			"legend": {
				"horizontalGap": 10,
				"maxColumns": 1,
				"position": "bottom",
				"useGraphSettings": true,
				"markerSize": 10,
				"marginTop": 20
			},
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-emissao-media.php?mesAno=" + localStorage.getItem('dados') + "&tipo=emissaoMesSig"
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-emissao-media.php?mesAno=" + localStorage.getItem('dados') + "&tipo=emissaoMesSig"
			},
			"valueAxes": [{
				"stackType": "regular",
				"position": "left",
				"title": "Comparativo de emissões de certificados",
			}],
			"startDuration": 1,
			"graphs": [{
				"balloonText": "Certificados emitidos no dia [[category]] do mês atual: <b>[[value]]</b>",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"title": "Mês Atual",
				"type": "column",
				"valueField": "atual"
			}, {
				"balloonText": "Certificados emitidos no dia [[category]] do mês anterior: <b>[[value]]</b>",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"title": "Mês Anterior",
				"type": "column",
				"valueField": "anterior"
			}],
			"plotAreaFillAlphas": 0.1,
			"categoryField": "dia",
			"categoryAxis": {
				"gridPosition": "start"
			},
			"export": {
				"enabled": true
			}
		});

		$('#chartMesAtualAnteriorSig').closest('.portlet').find('.fullscreen').click(function () {
			chart.invalidateSize();
		});
	}

	var initChartOriginacao = function () {
		var pieChart = AmCharts.makeChart("chartOriginacao", {
			"type": "funnel",
			"theme": "light",
			"titleField": "tipo",
			"valueField": "valor",
			"fontSize": 12,
			"allLabels": [],
			"titles": [],
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-emissao-media.php?mesAno=" + localStorage.getItem('dados') + "&tipo=originacao"
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-emissao-media.php?mesAno=" + localStorage.getItem('dados') + "&tipo=originacao"
			},
			"outlineAlpha": 0.4,
			"colorField": "color",
			"startDuration": 0,
			"balloon": {
				"fixedPosition": true
			},
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"marginRight": 240,
			"marginLeft": 50,
			"startX": -500,
			"depth3D": 100,
			"angle": 40,
			"outlineAlpha": 1,
			"outlineColor": "#FFFFFF",
			"outlineThickness": 2,
			"labelPosition": "right",
			"export": {
				"enabled": true
			}
		});
		$('#chartOriginacao').closest('.portlet').find('.fullscreen').click(function () {
			chart.invalidateSize();
		});
	}

	var initChartOriginacaoMesAtual = function () {
		var pieChart = AmCharts.makeChart("chartOriginacaoMesAtual", {
			"type": "funnel",
			"theme": "light",
			"titleField": "tipo",
			"valueField": "valor",
			"fontSize": 12,
			"allLabels": [],
			"titles": [],
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-emissao-media.php?mesAno=" + localStorage.getItem('dados') + "&tipo=originacaoMesAtual"
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-emissao-media.php?mesAno=" + localStorage.getItem('dados') + "&tipo=originacaoMesAtual"
			},
			"outlineAlpha": 0.4,
			"colorField": "color",
			"startDuration": 0,
			"balloon": {
				"fixedPosition": true
			},
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"marginRight": 240,
			"marginLeft": 50,
			"startX": -500,
			"depth3D": 100,
			"angle": 40,
			"outlineAlpha": 1,
			"outlineColor": "#FFFFFF",
			"outlineThickness": 2,
			"labelPosition": "right",
			"export": {
				"enabled": true
			}
		});
		$('#chartOriginacaoMesAtual').closest('.portlet').find('.fullscreen').click(function () {
			chart.invalidateSize();
		});
	}

	return {
		//main function to initiate the module
		init: function() {
			initChartEmissao();
			initChartEmissaoSig();
			initChartOriginacao();
			initChartOriginacaoMesAtual();
		}
	};
}();

var FormEditableItsSiteInformbank = function () {

	function respostaBD(response) {
		if (response == 'ok') {
			toastr.success('O registro foi alterado com sucesso!', 'Atualização Cadastral');
		} else {
			toastr.error('Houve um problema e o registro não pode ser alterado!', 'Atualização Cadastral');
		};

		if (response.status == 'error') return response.msg; //msg will be shown in editable form
	}

	var initEditables = function () {

		//global settings
		$.fn.editable.defaults.mode = 'inline';
		$.fn.editable.defaults.inputclass = 'form-control';
		$.fn.editable.defaults.url = 'assets/inc/form-editable-its-site-informbank.php';

		//editables element samples
		$('.nomeEmpresa').editable({
			type: 'text',
			name: 'nomeEmpresa',
			title: 'Nome Empresa',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		//editables element samples
		$('.nomeIts').editable({
			type: 'text',
			name: 'nomeIts',
			title: 'Nome ITS',
			emptytext: "Vazio",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.cnpj').editable({
			type: 'text',
			name: 'cnpj',
			title: 'CNPJ',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
				if (validarCnpj(value) == false) return '* CNPJ inválido.';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.email').editable({
			type: 'text',
			name: 'email',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
				if (validateEmail(value) == false) return '* Email inválido.';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.cnpjFilial').editable({
			type: 'text',
			name: 'cnpjFilial',
			title: 'CNPJ Filial',
			emptytext: "Vazio",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
				if (validarCnpj(value) == false) return '* CNPJ inválido.';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('#endereco').editable({
			validate: function (value) {
				if (value.logradouro == '') return 'Logradouro é obrigatório!';
				if (value.numero == '') return 'Número é obrigatório!';
			},
			display: function (value) {
				if (!value) {
					$(this).empty();
					return;
				}
				var html = '<b>' + $('<div>').text(value.logradouro).html() + '</b>, ' + $('<div>').text(value.numero).html() + ' ' + $('<div>').text(value.complemento).html();
				$(this).html(html);
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.bairro').editable({
			type: 'text',
			name: 'bairro',
			title: 'Bairro',
			emptytext: "Vazio",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.cidade').editable({
			type: 'text',
			name: 'cidade',
			title: 'Cidade',
			emptytext: "Vazio",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		var estados = [];
		$.each({
			"AC": "Acre",
			"AL": "Alagoas",
			"AP": "Amapá",
			"AM": "Amazonas",
			"BA": "Bahia",
			"CE": "Ceará",
			"DF": "Distrito Federal",
			"ES": "Espírito Santo",
			"GO": "Goiás",
			"MA": "Maranhão",
			"MT": "Mato Grosso",
			"MS": "Mato Grosso do Sul",
			"MG": "Minas Gerais",
			"PA": "Pará",
			"PB": "Paraíba",
			"PR": "Paraná",
			"PE": "Pernambuco",
			"PI": "Piauí",
			"RJ": "Rio de Janeiro",
			"RN": "Rio Grande do Norte",
			"RS": "Rio Grande do Sul",
			"RO": "Rondônia",
			"RR": "Roraima",
			"SC": "Santa Catarina",
			"SP": "São Paulo",
			"SE": "Sergipe",
			"TO": "Tocantins"
		}, function (k, v) {
			estados.push({
				id: k,
				text: v
			});
		});

		$('#uf').editable({
			inputclass: 'form-control input-medium',
			source: estados,
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.cep').editable({
			type: 'text',
			name: 'cep',
			title: 'CEP',
			emptytext: "Vazio",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.ambienteAtendimento').editable({
			showbuttons: true,
			type: "select",
			name: "ambienteAtendimento",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'compartilhado',
					text: 'Compartilhado'
				}, {
					value: 'dedicado',
					text: 'Dedicado'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.atividadePrincipal').editable({
			showbuttons: true,
			type: "select",
			name: "atividadePrincipal",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'advocacia',
					text: 'Advocacia'
				}, {
					value: 'associacao',
					text: 'Associação'
				}, {
					value: 'certificadora',
					text: 'Certificadora'
				}, {
					value: 'contabilidade',
					text: 'Contabilidade'
				}, {
					value: 'corretora',
					text: 'Corretora'
				}, {
					value: 'seguradora',
					text: 'Seguradora'
				}, {
					value: 'outro',
					text: 'Outro'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.tipoImovel').editable({
			showbuttons: true,
			type: "select",
			name: "tipoImovel",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'predio',
					text: 'Prédio'
				}, {
					value: 'sobreloja',
					text: 'Sobreloja'
				}, {
					value: 'terreo',
					text: 'Terreo'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.recepcionista').editable({
			showbuttons: true,
			type: "select",
			name: "recepcionista",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'sim',
					text: 'Sim'
				}, {
					value: 'nao',
					text: 'Não'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.salaEspera').editable({
			showbuttons: true,
			type: "select",
			name: "salaEspera",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'sim',
					text: 'Sim'
				}, {
					value: 'nao',
					text: 'Não'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.arCondicionado').editable({
			showbuttons: true,
			type: "select",
			name: "arCondicionado",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'sim',
					text: 'Sim'
				}, {
					value: 'nao',
					text: 'Não'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.bebidasCortesia').editable({
			showbuttons: true,
			type: "select",
			name: "bebidasCortesia",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'sim',
					text: 'Sim'
				}, {
					value: 'nao',
					text: 'Não'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.escadasAcesso').editable({
			showbuttons: true,
			type: "select",
			name: "escadasAcesso",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'sim',
					text: 'Sim'
				}, {
					value: 'nao',
					text: 'Não'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.elevador').editable({
			showbuttons: true,
			type: "select",
			name: "elevador",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'sim',
					text: 'Sim'
				}, {
					value: 'nao',
					text: 'Não'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.acessoEspeciais').editable({
			showbuttons: true,
			type: "select",
			name: "acessoEspeciais",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'sim',
					text: 'Sim'
				}, {
					value: 'nao',
					text: 'Não'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.estacionamento').editable({
			showbuttons: true,
			type: "select",
			name: "estacionamento",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'nao',
					text: 'Não'
				}, {
					value: 'sim-gratuito',
					text: 'Sim - Gratuito'
				}, {
					value: 'sim-pago',
					text: 'Sim - Pago'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.shoppingReferencia').editable({
			showbuttons: 'bottom',
			type: 'textarea',
			name: 'shoppingReferencia',
			emptytext: 'Vazio',
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.tremReferencia').editable({
			showbuttons: 'bottom',
			type: 'textarea',
			name: 'tremReferencia',
			emptytext: 'Vazio',
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.onibusReferencia').editable({
			showbuttons: 'bottom',
			type: 'textarea',
			name: 'onibusReferencia',
			emptytext: 'Vazio',
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.avenidaReferencia').editable({
			showbuttons: 'bottom',
			type: 'textarea',
			name: 'avenidaReferencia',
			emptytext: 'Vazio',
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.horarioAtendimento').editable({
			type: 'text',
			name: 'horarioAtendimento',
			title: 'Horário de Atendimento',
			emptytext: "Vazio",
			inputclass: 'form-control input-small',
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.focoAtendimento').editable({
			showbuttons: true,
			type: "select",
			name: "focoAtendimento",
			title: 'Foco do Atendimentp',
			emptytext: "Vazio",
			inputclass: 'form-control',
			source: [{
					value: 'clientes-especificos',
					text: 'Clientes Específicos'
				}, {
					value: 'publico-geral',
					text: 'Público em Geral'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.dispAgenda').editable({
			showbuttons: true,
			type: "select",
			name: "dispAgenda",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'agenda-aberta',
					text: 'Agenda Aberta'
				}, {
					value: 'agenda-fechada',
					text: 'Agenda Fechada'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.dispValidaExterna').editable({
			showbuttons: true,
			type: "select",
			name: "dispAgenda",
			inputclass: 'form-control',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			source: [{
					value: 'sim',
					text: 'Sim'
				}, {
					value: 'nao',
					text: 'Não'
				}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});


	}
	return {
		//main function to initiate the module
		init: function () {
			// init editable elements
			initEditables();
		}
	};
}();

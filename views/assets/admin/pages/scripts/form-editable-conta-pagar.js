var FormEditableContaPagar = function () {

	function respostaBD(response){
		if(response == 'ok') {
			toastr.success('O registro foi alterado com sucesso!', 'Atualização Cadastral');
		}else{
			toastr.error('Houve um problema e o registro não pode ser alterado!', 'Atualização Cadastral');
		};

		if(response.status == 'error') return response.msg; //msg will be shown in editable form
	}

	var initEditables = function () {

		//global settings
		$.fn.editable.defaults.mode = 'inline';
		$.fn.editable.defaults.inputclass = 'form-control';
		$.fn.editable.defaults.url = 'assets/inc/form-editable-conta-pagar.php';

		$('.nomeResponsavel').editable({
			showbuttons: true,
			type: "select",
			name: "nomeResponsavel",
			title: 'Nome Responsável',
			emptytext: "Vazio",
			source: [{
				value: '1',
				text: 'CERTBANK'
			}, {
				value: '2',
				text: 'INFORMBANK'
			}],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.dataVencimento').editable({
			type: 'date',
			name: 'dataVencimento',
			format: 'yyyy-mm-dd',
			viewformat: 'dd/mm/yyyy',
			datepicker: {
				rtl: Metronic.isRTL(),
				orientation: 'left',
				autoclose: true
			},
			emptytext: "Vazio",
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.historico').editable({
			showbuttons: 'bottom',
			type: 'textarea',
			name: 'historico',
			emptytext: "Vazio",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.numDoc').editable({
			type: 'text',
			name: 'numDoc',
			title: 'Número do documento',
			emptytext: "Vazio",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.valor').editable({
			type: 'text',
			name: 'valor',
			title: 'Valor',
			emptytext: "0.00",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
				if ($.trim(value) < 0) return 'Valor não pode ser negativo';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.juros').editable({
			type: 'text',
			name: 'valor',
			title: 'Valor',
			emptytext: "0.00",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
				if ($.trim(value) < 0) return 'Valor não pode ser negativo';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.multa').editable({
			type: 'text',
			name: 'valor',
			title: 'Valor',
			emptytext: "0.00",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
				if ($.trim(value) < 0) return 'Valor não pode ser negativo';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.desconto').editable({
			type: 'text',
			name: 'valor',
			title: 'Valor',
			emptytext: "0.00",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
				if ($.trim(value) < 0) return 'Valor não pode ser negativo';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.nf').editable({
			type: 'text',
			name: 'nf',
			title: 'Nota Fiscal',
			emptytext: "Vazio",
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			},
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.dataPagamento').editable({
			type: 'date',
			name: 'dataPagamento',
			format: 'yyyy-mm-dd',
			viewformat: 'dd/mm/yyyy',
			datepicker:{
				rtl: Metronic.isRTL(),
				orientation: 'left',
				autoclose: true
			},
			emptytext: "Vazio",
			success: function(response, newValue) {
				respostaBD(response);
			}
		});

		$('.formaPagamento').editable({
			showbuttons: true,
			type: "select",
			name: "formaPagamento",
			title: 'Forma Pagamento',
			emptytext: "Vazio",
			source: [{
				value: '1',
				text: 'Boleto Bancário'
			}, {
				value: '2',
				text: 'Cartão Crédito'
			}, {
				value: '3',
				text: 'Cartão Débito'
			}, {
				value: '4',
				text: 'Transferência'
			}, {
				value: '5',
				text: 'Fatura'
			}, {
				value: '6',
				text: 'Saque'
			}, {
				value: '7',
				text: 'Depósito'
			}, {
				value: '8',
				text: 'Débito Automático'
			}, {
				value: '9',
				text: 'Dinheiro'
			}
			],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.nomeEmpresa').editable({
			showbuttons: true,
			type: "select",
			name: "nomeEmpresa",
			title: 'Nome Empresa',
			emptytext: "Vazio",
			source: [{
				value: '1',
				text: 'CERTBANK'
			}, {
				value: '2',
				text: 'INFORMBANK'
			}],
			success: function (response, newValue) {
				respostaBD(response);
			}
		});


		$('.bancoPagamento1').editable({
			source: 'assets/inc/lista-conta-bancaria.php?idempresa=1',
			showbuttons: true,
			type: "select",
			name: "bancoPagamento",
			title: 'Banco de Pagamento',
			emptytext: "Vazio",
			inputclass: 'form-control input-small',
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.bancoPagamento2').editable({
			source: 'assets/inc/lista-conta-bancaria.php?idempresa=2',
			showbuttons: true,
			type: "select",
			name: "bancoPagamento",
			title: 'Banco de Pagamento',
			emptytext: "Vazio",
			inputclass: 'form-control input-small',
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

		$('.centroCusto').editable({
			source: 'assets/inc/lista-centro-custo.php',
			showbuttons: true,
			type: "select",
			name: "centroCusto",
			title: 'Centro de Custo',
			emptytext: "Vazio",
			inputclass: 'form-control input-small',
			success: function (response, newValue) {
				respostaBD(response);
			}
		});

	}
	return {
		//main function to initiate the module
		init: function () {
		// init editable elements
		initEditables();
		}
	};
}();

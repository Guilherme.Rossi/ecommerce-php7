var Index = function () {

	return {

		initCalendar: function () {
			if (!jQuery().fullCalendar) {
				return;
			}

			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();

			var h = {};

			var zone = "03:00"; //Change this to your timezone

			$.ajax({
				url: 'assets/inc/calendar-process.php',
				type: 'POST', // Send post data
				data: 'type=fetch',
				async: false,
				success: function (s) {
					json_events = s;
				}
			});

			var currentMousePos = {
				x: -1,
				y: -1
			};
			jQuery(document).on("mousemove", function (event) {
				currentMousePos.x = event.pageX;
				currentMousePos.y = event.pageY;
			});

			/* initialize the external events
			-----------------------------------------------------------------*/

			$('#external-events .fc-event').each(function () {

				// store data so the calendar knows to render an event upon drop
				$(this).data('event', {
					title: $.trim($(this).text()), // use the element's text as the event title
					stick: true // maintain when user navigates (see docs on the renderEvent method)
				});

				// make the event draggable using jQuery UI
				$(this).draggable({
					zIndex: 999,
					revert: true, // will cause the event to go back to its
					revertDuration: 0 //  original position after the drag
				});

			});

			function getFreshEvents() {
				$.ajax({
					url: 'assets/inc/calendar-process.php',
					type: 'POST', // Send post data
					data: 'type=fetch',
					async: false,
					success: function (s) {
						freshevents = s;
					}
				});
				$('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
			}

			function isElemOverDiv() {
				var trashEl = jQuery('#trash');

				var ofs = trashEl.offset();

				var x1 = ofs.left;
				var x2 = ofs.left + trashEl.outerWidth(true);
				var y1 = ofs.top;
				var y2 = ofs.top + trashEl.outerHeight(true);

				if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
					currentMousePos.y >= y1 && currentMousePos.y <= y2) {
					return true;
				}
				return false;
			};

			if ($('#calendar').width() <= 400) {
				$('#calendar').addClass("mobile");
				h = {
					left: 'title, prev, next',
					center: '',
					right: 'today,month,agendaWeek,agendaDay'
				};
			} else {
				$('#calendar').removeClass("mobile");
				if (Metronic.isRTL()) {
					h = {
						right: 'title',
						center: '',
						left: 'prev,next,today,month,agendaWeek,agendaDay'
					};
				} else {
					h = {
						left: 'title',
						center: '',
						right: 'prev,next,today,month,agendaWeek,agendaDay'
					};
				}
			}

			$('#calendar').fullCalendar('destroy'); // destroy the calendar
			$('#calendar').fullCalendar({ //re-initialize the calendar
				locale: 'pt-br',
				disableDragging: false,
				events: JSON.parse(json_events),
				utc: true,
				header: h,
				editable: true,
				droppable: true,
				slotDuration: '00:30:00',
				eventReceive: function (event) {
					var title = event.title;
					var start = event.start.format("YYYY-MM-DD[T]HH:mm:SS");
					$.ajax({
						url: 'assets/inc/calendar-process.php',
						data: 'type=new&title=' + title + '&startdate=' + start + '&zone=' + zone,
						type: 'POST',
						dataType: 'json',
						success: function (response) {
							event.id = response.eventid;
							$('#calendar').fullCalendar('updateEvent', event);
						},
						error: function (e) {
							console.log(e.responseText);

						}
					});
					$('#calendar').fullCalendar('updateEvent', event);
					console.log(event);
				},
				eventDrop: function (event, delta, revertFunc) {
					var title = event.title;
					var start = event.start.format();
					var end = (event.end == null) ? start : event.end.format();
					$.ajax({
						url: 'assets/inc/calendar-process.php',
						data: 'type=resetdate&title=' + title + '&start=' + start + '&end=' + end + '&eventid=' + event.id,
						type: 'POST',
						dataType: 'json',
						success: function (response) {
							if (response.status != 'success')
								revertFunc();
						},
						error: function (e) {
							revertFunc();
							alert('Error processing your request: ' + e.responseText);
						}
					});
				},
				eventClick: function (event, jsEvent, view) {
					console.log(event.id);
					var title = prompt('Título do Evento:', event.title, {
						buttons: {
							Ok: true,
							Cancel: false
						}
					});
					if (title) {
						event.title = title;
						console.log('type=changetitle&title=' + title + '&eventid=' + event.id);
						$.ajax({
							url: 'assets/inc/calendar-process.php',
							data: 'type=changetitle&title=' + title + '&eventid=' + event.id,
							type: 'POST',
							dataType: 'json',
							success: function (response) {
								if (response.status == 'success')
									$('#calendar').fullCalendar('updateEvent', event);
							},
							error: function (e) {
								alert('Error processing your request: ' + e.responseText);
							}
						});
					}
				},
				eventResize: function (event, delta, revertFunc) {
					console.log(event);
					var title = event.title;
					var end = event.end.format();
					var start = event.start.format();
					$.ajax({
						url: 'assets/inc/calendar-process.php',
						data: 'type=resetdate&title=' + title + '&start=' + start + '&end=' + end + '&eventid=' + event.id,
						type: 'POST',
						dataType: 'json',
						success: function (response) {
							if (response.status != 'success')
								revertFunc();
						},
						error: function (e) {
							revertFunc();
							alert('Error processing your request: ' + e.responseText);
						}
					});
				},
				eventDragStop: function (event, jsEvent, ui, view) {
					if (isElemOverDiv()) {
						var con = confirm('Deseja apagar este evento permanentemente?');
						if (con == true) {
							$.ajax({
								url: 'assets/inc/calendar-process.php',
								data: 'type=remove&eventid=' + event.id,
								type: 'POST',
								dataType: 'json',
								success: function (response) {
									console.log(response);
									if (response.status == 'success') {
										$('#calendar').fullCalendar('removeEvents');
										getFreshEvents();
									}
								},
								error: function (e) {
									alert('Error processing your request: ' + e.responseText);
								}
							});
						}
					}
				}
			});
		},

		initMiniCharts: function () {
			if (!jQuery().easyPieChart || !jQuery().sparkline) {
				return;
			}

			// IE8 Fix: function.bind polyfill
			if (Metronic.isIE8() && !Function.prototype.bind) {
				Function.prototype.bind = function (oThis) {
					if (typeof this !== "function") {
						// closest thing possible to the ECMAScript 5 internal IsCallable function
						throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
					}

					var aArgs = Array.prototype.slice.call(arguments, 1),
						fToBind = this,
						fNOP = function () {},
						fBound = function () {
							return fToBind.apply(this instanceof fNOP && oThis ? this : oThis,
								aArgs.concat(Array.prototype.slice.call(arguments)));
						};

					fNOP.prototype = this.prototype;
					fBound.prototype = new fNOP();

					return fBound;
				};
			}

			$('.easy-pie-chart .number.transactions').easyPieChart({
				animate: 1000,
				size: 75,
				lineWidth: 3,
				barColor: Metronic.getBrandColor('green')
			});

			$('.easy-pie-chart .number.visits').easyPieChart({
				animate: 1000,
				size: 75,
				lineWidth: 3,
				barColor: Metronic.getBrandColor('yellow')
			});

			$('.easy-pie-chart .number.bounce').easyPieChart({
				animate: 1000,
				size: 75,
				lineWidth: 3,
				barColor: Metronic.getBrandColor('blue')
			});

			$("#sparkline_bar").sparkline('html', {
				type: 'bar',
				width: '100',
				barWidth: 10,
				height: '55',
				barColor: '#35aa47',
				negBarColor: '#e02222'
			});

			$("#sparkline_bar2").sparkline('html', {
				type: 'bar',
				width: '100',
				barWidth: 10,
				height: '55',
				barColor: '#30ACCF',
				negBarColor: '#e02222'
			});

			$("#sparkline_line").sparkline('html', {
				type: 'line',
				width: '100',
				height: '55',
				lineColor: '#655',
				fillColor: '#30ACCF'
			});

		}

	};

}();

var RelContasPAgar = function () {
	return {
		//main function to initiate the module
		init: function () {
			$("#nomePessoa").select2();
			if (jQuery().datepicker) {
				$('.date-picker').datepicker({
					language: 'pt-BR'
				});
			}
		}
	};
}();

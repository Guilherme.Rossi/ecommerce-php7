var ChartsAmcharts = function() {
	var initChartGrupoRankingPizza = function() {
		var pieChart = AmCharts.makeChart("chartRankingTipoPizza", {
			"type": "pie",
			"theme": "light",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"titleField": "tipo",
			"valueField": "valor",
			"fontSize": 12,
			"allLabels": [],
			"balloon": {},
			"titles": [],
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-ranking-tipo.php?mesAno="+mesAno+"&tipo=contaPagarPizza&estado="+estado
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-ranking-tipo.php?mesAno="+mesAno+"&tipo=contaPagarPizza&estado="+estado
			},
			"outlineAlpha": 0.4,
			"depth3D": 20,
			"angle": 30,
			"colorField": "color"
		});
		$('#chartRankingTipoPizza').closest('.portlet').find('.fullscreen').click(function() {
			chart.invalidateSize();
		});
	}

	var initChartHistoricoPonto = function () {
		var chart = AmCharts.makeChart("chartHistoricoPonto", {
			"type": "serial",
			"theme": "light",
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-ranking-tipo.php?tipo=historicoPonto&mesAno=" + mesAno
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-ranking-tipo.php?tipo=historicoPonto&mesAno=" + mesAno
			},
			"valueAxes": [{
				"gridColor": "#FFFFFF",
				"gridAlpha": 0.2,
				"dashLength": 0
			}],
			"gridAboveGraphs": true,
			"startDuration": 1,
			"graphs": [{
				"balloonText": "[[category]]: <b>[[value]]</b>",
				"fillAlphas": 0.8,
				"lineAlpha": 0.2,
				"type": "column",
				"valueField": "valor"
			}],
			"chartCursor": {
				"categoryBalloonEnabled": false,
				"cursorAlpha": 0,
				"zoomable": false
			},
			"categoryField": "tipo",
			"categoryAxis": {
				"autoRotateAngle": 45,
				"autoRotateCount": 0,
				"gridPosition": "start",
				"gridAlpha": 0,
				"tickPosition": "start",
				"tickLength": 20
			},
			"export": {
				"enabled": true
			}
		});
		$('#chartContaPagarBarra').closest('.portlet').find('.fullscreen').click(function () {
			chart.invalidateSize();
		});
	}

	var initChartTipoEmissao = function () {
		var pieChart = AmCharts.makeChart("chartTipoEmissao", {
			"type": "pie",
			"theme": "light",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"titleField": "tipo",
			"valueField": "valor",
			"fontSize": 12,
			"allLabels": [],
			"balloon": {},
			"titles": [],
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-ranking-tipo.php?&tipo=tipoEmissao&mesAno=" + mesAno + "&idLote=" + estado
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-ranking-tipo.php?&tipo=tipoEmissao&mesAno=" + mesAno + "&idLote=" + estado
			},
			"outlineAlpha": 0.4,
			"depth3D": 20,
			"angle": 30,
			"colorField": "color"
		});
		$('#chartTipoEmissao').closest('.portlet').find('.fullscreen').click(function () {
			chart.invalidateSize();
		});
	}

	var initChartGrupoRankingPizzaRede = function () {
		var pieChart = AmCharts.makeChart("chartRankingTipoPizzaRede", {
			"type": "pie",
			"theme": "light",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"titleField": "tipo",
			"valueField": "valor",
			"fontSize": 12,
			"allLabels": [],
			"balloon": {},
			"titles": [],
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-ranking-tipo.php?&tipo=RankingTipoPizzaRede&mesAno=" + mesAno
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-ranking-tipo.php?&tipo=RankingTipoPizzaRede&mesAno=" + mesAno
			},
			"outlineAlpha": 0.4,
			"depth3D": 20,
			"angle": 30,
			"colorField": "color"
		});
		$('#chartRankingTipoPizzaRede').closest('.portlet').find('.fullscreen').click(function () {
			chart.invalidateSize();
		});
	}
	return {
		//main function to initiate the module
		init: function() {
			initChartGrupoRankingPizza();
			initChartHistoricoPonto();
			initChartTipoEmissao();
			initChartGrupoRankingPizzaRede();
		}
	};
}();

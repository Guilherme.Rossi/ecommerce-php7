var FormWizardCriaPonto = function () {

		$.validator.addMethod("cpfcnpj", brdocs.cpfcnpjValidator, "Informe um documento válido.");

		return {
				//main function to initiate the module
				init: function () {
						if (!jQuery().bootstrapWizard) {
								return;
						}

						$("#consultor_list").select2();
						$("#supervisor_list").select2();
						$("#gerente_list").select2();
						$("#superintendente_list").select2();

						$("#listaEstados").select2({
							placeholder: "Select",
							allowClear: true,
							escapeMarkup: function (m) {
								return m;
							}
						});

						var form = $('#submit_form');
						var error = $('.alert-danger', form);
						var success = $('.alert-success', form);

						form.validate({
								doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
								errorElement: 'span', //default input error message container
								errorClass: 'help-block help-block-error', // default input error message class
								focusInvalid: false, // do not focus the last invalid input
								rules: {
									//dados ponto
									nomePonto: {
										minlength: 5,
										required: true
									},
									cnpj: {
										required: true/*,
										cpfcnpj: brdocs.cpfcnpj.CNPJ*/
									},
									nomeEmpresa: {
										minlength: 5,
										required: true
									},
									email: {
										required: true,
										email: true,
										maxlength: 50
									},
									foneComercial: {
										required: true,
										maxlength: 14
									},
									//endereço ponto
									logradouro: {
										required: true
									},
									numero: {
										required: true
									},
									bairro: {
										required: true
									},
									cidade: {
										required: true
									},
									estado: {
										required: true
									},
									cep: {
										required: true,
										maxlength: 9
									},
									//contrato
									grupoComissao: {
										required: true
									},
									idpessoa_pai: {
										required: true
									},
									'optionsTED': {
											required: true
									},
									'optionsAgente': {
											required: true
									}
								},

								messages: { // custom messages for radio buttons and checkboxes
										'optionsTED': {
											required: "Favor escolher uma opção",
											minlength: jQuery.validator.format("Favor escolher uma opção")
										},
										'optionsAgente': {
											required: "Favor escolher uma opção",
											minlength: jQuery.validator.format("Favor escolher uma opção")
										}
								},

								errorPlacement: function (error, element) { // render error placement for each input type
										if (element.attr("name") == "optionsTED") { // for uniform radio buttons, insert the after the given container
												error.insertAfter("#form_ted_error");
										} else if (element.attr("name") == "optionsAgente") { // for uniform checkboxes, insert the after the given container
												error.insertAfter("#form_agente_error");
										} else {
												error.insertAfter(element); // for other inputs, just perform default behavior
										}
								},

								invalidHandler: function (event, validator) { //display error alert on form submit
										success.hide();
										error.show();
										Metronic.scrollTo(error, -200);
								},

								highlight: function (element) { // hightlight error inputs
										$(element)
												.closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
								},

								unhighlight: function (element) { // revert the change done by hightlight
										$(element)
												.closest('.form-group').removeClass('has-error'); // set error class to the control group
								},

								success: function (label) {
										if (label.attr("for") == "optionsTED" || label.attr("for") == "optionsAgente") { // for checkboxes and radio buttons, no need to show OK icon
												label
														.closest('.form-group').removeClass('has-error').addClass('has-success');
												label.remove(); // remove error label here
										} else { // display success icon for other inputs
												label
														.addClass('valid') // mark the current input as valid and display OK icon
												.closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
										}
								},

								submitHandler: function (form) {
										success.show();
										error.hide();
										$('#submit_form').submit();
								}

						});

						var displayConfirm = function() {
								$('#tab4 .form-control-static', form).each(function(){
										var input = $('[name="'+$(this).attr("data-display")+'"]', form);
										if (input.is(":radio")) {
											input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
										}
										if (input.is(":text") || input.is("textarea")) {
											$(this).html(input.val());
										} else if (input.is("select")) {
											$(this).html(input.val());
											$(this).html(input.find('option:selected').text());
										} else if (input.is(":radio") && input.is(":checked")) {
											$(this).html(input.val());
											$(this).html(input.attr("data-title"));
										}
								});
						}

						var handleTitle = function(tab, navigation, index) {
								var total = navigation.find('li').length;
								var current = index + 1;
								// set wizard title
								$('.step-title', $('#form_wizard_1')).text('Passo ' + (index + 1) + ' de ' + total);
								// set done steps
								jQuery('li', $('#form_wizard_1')).removeClass("done");
								var li_list = navigation.find('li');
								for (var i = 0; i < index; i++) {
										jQuery(li_list[i]).addClass("done");
								}

								if (current == 1) {
										$('#form_wizard_1').find('.button-previous').hide();
								} else {
										$('#form_wizard_1').find('.button-previous').show();
								}

								if (current >= total) {
										$('#form_wizard_1').find('.button-next').hide();
										$('#form_wizard_1').find('.button-submit').show();
										displayConfirm();
								} else {
										$('#form_wizard_1').find('.button-next').show();
										$('#form_wizard_1').find('.button-submit').hide();
								}
								Metronic.scrollTo($('.page-title'));
						}

						// default form wizard
						$('#form_wizard_1').bootstrapWizard({
								'nextSelector': '.button-next',
								'previousSelector': '.button-previous',
								onTabClick: function (tab, navigation, index, clickedIndex) {
										return false;
										/*
										success.hide();
										error.hide();
										if (form.valid() == false) {
												return false;
										}
										handleTitle(tab, navigation, clickedIndex);
										*/
								},
								onNext: function (tab, navigation, index) {
										success.hide();
										error.hide();

										if (form.valid() == false) {
												return false;
										}

										handleTitle(tab, navigation, index);
								},
								onPrevious: function (tab, navigation, index) {
										success.hide();
										error.hide();

										handleTitle(tab, navigation, index);
								},
								onTabShow: function (tab, navigation, index) {
										var total = navigation.find('li').length;
										var current = index + 1;
										var $percent = (current / total) * 100;
										$('#form_wizard_1').find('.progress-bar').css({
												width: $percent + '%'
										});
								}
						});

						$('#form_wizard_1').find('.button-previous').hide();
						$('#form_wizard_1 .button-submit').click(function () {
								//alert('Finished! Hope you like it :)');
						}).hide();

						//apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
						$('#listaEstados', form).change(function () {
								form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
						});
				}

		};

}();

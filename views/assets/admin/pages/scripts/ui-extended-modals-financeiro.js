var UIExtendedModals = function () {

	return {
		//main function to initiate the module
		init: function () {

			// general settings
			$.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
				'<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
					'<div class="progress progress-striped active">' +
						'<div class="progress-bar" style="width: 100%;"></div>' +
					'</div>' +
				'</div>';

			$.fn.modalmanager.defaults.resize = true;

			var $modal = $('#financeiro-modal');

			$('.confirma-pagto-modal').on('click', function(){
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idconta = $(this).data("idconta");
				valorpago = $(this).data("valorpago");

				setTimeout(function(){
					$modal.load('assets/inc/ui_extended_modals_financeiro.php', 'idconta='+idconta+'&valorpago='+valorpago+'&tipo=confirma-pagto', function(){
					$modal.modal();
					});
				}, 1000);
			});

			$('.cancela-pagto-modal').on('click', function(){
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idconta = $(this).data("idconta");
				idempresa = $(this).data("idempresa");
				valorpago = $(this).data("valorpago");

				setTimeout(function(){
					$modal.load('assets/inc/ui_extended_modals_financeiro.php', 'idconta='+idconta+'&idempresa='+idempresa+'&valorpago='+valorpago+'&tipo=cancela-pagto', function(){
					$modal.modal();
					});
				}, 1000);
			});

			$('.confirma-recebimento-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idconta = $(this).data("idconta");
				valorrecebido = $(this).data("valorrecebido");
				banco = $(this).data("banco");

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_financeiro.php', 'idconta=' + idconta + '&valorrecebido=' + valorrecebido + '&banco=' + banco + '&tipo=confirma-recebimento', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.cancela-recebimento-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idconta = $(this).data("idconta");

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_financeiro.php', 'idconta=' + idconta + '&tipo=cancela-recebimento', function () {
						$modal.modal();
					});
				}, 1000);
			});

		}

	};

}();

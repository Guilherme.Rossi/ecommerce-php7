var UIExtendedModals = function () {

	return {
		//main function to initiate the module
		init: function () {

			// general settings
			$.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
				'<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
					'<div class="progress progress-striped active">' +
						'<div class="progress-bar" style="width: 100%;"></div>' +
					'</div>' +
				'</div>';

			$.fn.modalmanager.defaults.resize = true;

			var $modal = $('.egoi-modal');

			$('.deletar-modal').on('click', function(){
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idlista = $(this).data('idlista');
				setTimeout(function(){
					$modal.load('assets/inc/ui_extended_modals_egoi.php', 'idlista='+idlista+'&tipo=deletar-lista', function(){
					$modal.modal();
					});
				}, 1000);
			});

			$('.deletar-lista-contatos-modal').on('click', function(){
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				listacontato = $(this).data('listacontato');
				setTimeout(function(){
					$modal.load('assets/inc/ui_extended_modals_egoi.php', 'listacontato='+listacontato+'&tipo=deletar-lista-contatos', function(){
					$modal.modal();
					});
				}, 1000);
			});

			$('.criar-lista-contatos-modal').on('click', function(){
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				codponto = $(this).data('codponto');
				mesano = $(this).data('mesano');
				setTimeout(function(){
					$modal.load('assets/inc/ui_extended_modals_egoi.php', 'codponto='+codponto+'&mesano='+mesano+'&tipo=criar-lista-contatos', function(){
					$modal.modal();
					});
				}, 1000);
			});

			$('.criar-lista-renovacao-modal').on('click', function(){
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				mesano = $(this).data('mesano');
				setTimeout(function(){
					$modal.load('assets/inc/ui_extended_modals_egoi.php', 'mesano='+mesano+'&tipo=criar-lista-renovacao', function(){
					$modal.modal();
					});
				}, 1000);
			});

			$('.atualizar-lista-contatos-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				codponto = $(this).data('codponto');
				mesano = $(this).data('mesano');
				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_egoi.php', 'codponto='+codponto+'&mesano='+mesano+'&tipo=atualizar-lista-contatos', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.criar-lista-vazia-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_egoi.php', '&tipo=criar-lista-vazia', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.atualiazar-lista-agentes-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_egoi.php', '&tipo=atualiazar-lista-agentes', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.adicionar-lista-agentes-desabilitados-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_egoi.php', '&tipo=adicionar-lista-agentes-desabilitados', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.deletar-unico-usuario-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idContato = $(this).data('hashcontato');
				idLista = $(this).data('listacontato');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_egoi.php', 'idcontato=' + idContato + '&idlista=' + idLista +'&tipo=deletar-usuario-unico', function () {
						$modal.modal();
					});
				}, 1000);
			});
		}

	};

}();

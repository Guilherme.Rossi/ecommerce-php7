var FormEditable = function () {

	var initEditables = function () {

		//global settings
		$.fn.editable.defaults.mode = 'inline';
		$.fn.editable.defaults.inputclass = 'form-control';
		$.fn.editable.defaults.url = 'assets/inc/form-editable-agente.php';

		//editables element samples
		$('#username').editable({
			type: 'text',
			name: 'username',
			title: 'Enter username',
			validate: function (value) {
				if ($.trim(value) == '') return 'Este campo é obrigatório';
			}
		});

		$('#sex').editable({
			prepend: "Não selecionado",
			inputclass: 'form-control',
			source: [{
					value: 1,
					text: 'Male'
				}, {
					value: 2,
					text: 'Female'
				}
			],
			display: function (value, sourceData) {
				var colors = {
					"": "gray",
					1: "green",
					2: "blue"
				},
				elem = $.grep(sourceData, function (o) {
					return o.value == value;
				});

				if (elem.length) {
					$(this).text(elem[0].text).css("color", colors[value]);
				} else {
					$(this).empty();
				}
			}
		});

		$('#group').editable({
			showbuttons: true,
			type: "select",
			emptytext: "Grupo"
		});

		$('#event').editable({
			placement: (Metronic.isRTL() ? 'left' : 'right'),
			combodate: {
				firstItem: 'name'
			}
		});

		$('#dob').editable({
			inputclass: 'form-control',
		});

		$('#vacation').editable({
			rtl : Metronic.isRTL()
		});

		$('#meeting_start').editable({
			format: 'yyyy-mm-dd hh:ii',
			viewformat: 'dd/mm/yyyy hh:ii',
			validate: function (v) {
				if (v && v.getDate() == 10) return 'Day cant be 10!';
			},
			datetimepicker: {
				rtl : Metronic.isRTL(),
				todayBtn: 'linked',
				weekStart: 1
			}
		});

		$('#comments').editable({
			showbuttons: 'bottom'
		});

		$('#fruits').editable({
			pk: 1,
			limit: 3,
			source: [{
					value: 1,
					text: 'banana'
				}, {
					value: 2,
					text: 'peach'
				}, {
					value: 3,
					text: 'apple'
				}, {
					value: 4,
					text: 'watermelon'
				}, {
					value: 5,
					text: 'orange'
				}
			]
		});

		$('#tags').editable({
			inputclass: 'form-control input-medium',
			select2: {
				tags: ['html', 'javascript', 'css', 'ajax'],
				tokenSeparators: [",", " "]
			}
		});

		var countries = [];
		$.each({
			"BD": "Bangladesh",
			"BE": "Belgium",
			"BR": "Brazil",
			"BS": "Bahamas",
			"JE": "Jersey",
			"BY": "Belarus",
			"O1": "Other Country",
			"LV": "Latvia",
			"RW": "Rwanda",
			"RS": "Serbia",
			"TL": "Timor-Leste",
			"RE": "Reunion",
			"LU": "Luxembourg",
			"TJ": "Tajikistan",
			"RO": "Romania",
			"PG": "Papua New Guinea",
			"GW": "Guinea-Bissau",
			"GU": "Guam",
			"GT": "Guatemala",
			"GS": "South Georgia and the South Sandwich Islands",
			"MQ": "Martinique",
			"MP": "Northern Mariana Islands",
			"MS": "Montserrat",
			"MR": "Mauritania",
			"IM": "Isle of Man",
			"MZ": "Mozambique"
		}, function (k, v) {
			countries.push({
				id: k,
				text: v
			});
		});

		$('#country').editable({
			inputclass: 'form-control input-medium',
			source: countries
		});

		$('#address').editable({
			validate: function (value) {
				if (value.logradouro == '') return 'Logradouro é obrigatório!';
			},
			display: function (value) {
				if (!value) {
					$(this).empty();
					return;
				}
				var html = '<b>' + $('<div>').text(value.logradouro).html() + '</b>, ' + $('<div>').text(value.numero).html() + ' ' + $('<div>').text(value.complemento).html();
				$(this).html(html);
			}
		});

		$('#frutas').editable({
			showbuttons: true,
			type: "checklist",
			pk: 1
		});
	}
	return {
		//main function to initiate the module
		init: function () {
			// init editable elements
			initEditables();
		}
	};
}();

var UIBootstrapGrowl = function() {

    return {
        //main function to initiate the module
        init: function() {

            setTimeout(function() {
                $.bootstrapGrowl("Processo concluído com Sucesso!", {
                    type: 'success',
                    delay: 10000
                });
            }, 500);

        }

    };

}();

var ChartsAmcharts = function() {
	var initChartGrupoPizza = function() {
		var pieChart = AmCharts.makeChart("estoqueMidiaUtilizada", {
			"type": "pie",
			"theme": "light",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"titleField": "tipo",
			"valueField": "valor",
			"fontSize": 12,
			"allLabels": [],
			"balloon": {},
			"titles": [],
			"dataLoader": {
				"url": "https://adm.certbank.com.br/assets/json/grafico-estoque.php?tipo=estoqueMidiaUtilizada&estado="+estado
				//"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-estoque.php?tipo=estoqueMidiaUtilizada&estado="+estado
			},
			"outlineAlpha": 0.4,
			"depth3D": 20,
			"angle": 30,
			"colorField": "color"
		});
		$('#estoqueMidiaUtilizada').closest('.portlet').find('.fullscreen').click(function() {
			chart.invalidateSize();
		});
	}

	return {
		//main function to initiate the module
		init: function() {
			initChartGrupoPizza();
		}
	};
}();

var FormEditable = function () {

		function respostaBD(response){
				if(response == 'ok') {
						toastr.success('O registro foi alterado com sucesso!', 'Atualização Cadastral');
				}else{
						toastr.error('Houve um problema e o registro não pode ser alterado!', 'Atualização Cadastral');
				};

				if(response.status == 'error') return response.msg; //msg will be shown in editable form
		}

		var initEditables = function () {

				//global settings
				$.fn.editable.defaults.mode = 'inline';
				$.fn.editable.defaults.inputclass = 'form-control';
				$.fn.editable.defaults.url = 'assets/inc/form-editable-ponto.php';

				//editables element samples
				$('.nomePonto').editable({
						type: 'text',
						name: 'nomePonto',
						title: 'Nome Ponto',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				//editables element samples
				$('.razaoSocial').editable({
						type: 'text',
						name: 'razaoSocial',
						title: 'Razão Social',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				//editables element samples
				$('.respNome').editable({
						type: 'text',
						name: 'respNome',
						title: 'Nome do Responsável',
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				//editables element samples
				$('.respCPF').editable({
						type: 'text',
						name: 'documento',
						title: 'Documento Responsável',
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				//editables element samples
				$('.respContato').editable({
						type: 'text',
						name: 'respContato',
						title: 'Contato Responsável',
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				$('.cnpjIts').editable({
						type: 'text',
						name: 'cnpjIts',
						title: 'CNPJ ITS',
						emptytext: "Vazio",
						validate: function (value) {
							if ($.trim(value) == '') return 'Este campo é obrigatório';
							if (validarCnpj(value) == false) return '* CNPJ inválido.';
						},
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				$('.nomeIts').editable({
						type: 'text',
						name: 'nomeIts',
						title: 'Nome ITS',
						emptytext: "Vazio",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				$('.cnpj').editable({
						type: 'text',
						name: 'cnpj',
						title: 'CNPJ',
						validate: function (value) {
							if ($.trim(value) == '') return 'Este campo é obrigatório';
							if (validarCnpj(value) == false) return '* CNPJ inválido.';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.codOrigem').editable({
						type: 'text',
						name: 'codOrigem',
						title: 'Código Origem',
						emptytext: "Vazio",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.codPonto').editable({
						type: 'text',
						name: 'codPonto',
						title: 'Código Ponto',
						emptytext: "Vazio",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('#enderecoCnpj').editable({
					validate: function (value) {
						if (value.logradouro == '') return 'Logradouro é obrigatório!';
						if (value.numero == '') return 'Número é obrigatório!';
					},
					display: function (value) {
						if (!value) {
							$(this).empty();
							return;
						}
						var html = '<b>' + $('<div>').text(value.logradouro).html() + '</b>, ' + $('<div>').text(value.numero).html() + ' ' + $('<div>').text(value.complemento).html();
						$(this).html(html);
					},
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				$('.bairroCnpj').editable({
					type: 'text',
					name: 'bairro',
					title: 'Bairro',
					emptytext: "Vazio",
					validate: function (value) {
						if ($.trim(value) == '') return 'Este campo é obrigatório';
					},
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				$('.cidadeCnpj').editable({
					type: 'text',
					name: 'cidade',
					title: 'Cidade',
					emptytext: "Vazio",
					validate: function (value) {
						if ($.trim(value) == '') return 'Este campo é obrigatório';
					},
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				var estadosCnpj = [];
				$.each({
					"AC": "Acre",
					"AL": "Alagoas",
					"AP": "Amapá",
					"AM": "Amazonas",
					"BA": "Bahia",
					"CE": "Ceará",
					"DF": "Distrito Federal",
					"ES": "Espírito Santo",
					"GO": "Goiás",
					"MA": "Maranhão",
					"MT": "Mato Grosso",
					"MS": "Mato Grosso do Sul",
					"MG": "Minas Gerais",
					"PA": "Pará",
					"PB": "Paraíba",
					"PR": "Paraná",
					"PE": "Pernambuco",
					"PI": "Piauí",
					"RJ": "Rio de Janeiro",
					"RN": "Rio Grande do Norte",
					"RS": "Rio Grande do Sul",
					"RO": "Rondônia",
					"RR": "Roraima",
					"SC": "Santa Catarina",
					"SP": "São Paulo",
					"SE": "Sergipe",
					"TO": "Tocantins"
				}, function (k, v) {
					estadosCnpj.push({
						id: k,
						text: v
					});
				});

				$('#ufCnpj').editable({
					inputclass: 'form-control input-medium',
					source: estadosCnpj,
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				$('.cepCnpj').editable({
					type: 'text',
					name: 'cep',
					title: 'CEP',
					emptytext: "Vazio",
					validate: function (value) {
						if ($.trim(value) == '') return 'Este campo é obrigatório';
					},
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				$('#endereco').editable({
						validate: function (value) {
								if (value.logradouro == '') return 'Logradouro é obrigatório!';
								if (value.numero == '') return 'Número é obrigatório!';
						},
						display: function (value) {
								if (!value) {
										$(this).empty();
										return;
								}
								var html = '<b>' + $('<div>').text(value.logradouro).html() + '</b>, ' + $('<div>').text(value.numero).html() + ' ' + $('<div>').text(value.complemento).html();
								$(this).html(html);
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.bairro').editable({
						type: 'text',
						name: 'bairro',
						title: 'Bairro',
						emptytext: "Vazio",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.cidade').editable({
						type: 'text',
						name: 'cidade',
						title: 'Cidade',
						emptytext: "Vazio",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				var estados = [];
				$.each({
					"AC": "Acre",
					"AL": "Alagoas",
					"AP": "Amapá",
					"AM": "Amazonas",
					"BA": "Bahia",
					"CE": "Ceará",
					"DF": "Distrito Federal",
					"ES": "Espírito Santo",
					"GO": "Goiás",
					"MA": "Maranhão",
					"MT": "Mato Grosso",
					"MS": "Mato Grosso do Sul",
					"MG": "Minas Gerais",
					"PA": "Pará",
					"PB": "Paraíba",
					"PR": "Paraná",
					"PE": "Pernambuco",
					"PI": "Piauí",
					"RJ": "Rio de Janeiro",
					"RN": "Rio Grande do Norte",
					"RS": "Rio Grande do Sul",
					"RO": "Rondônia",
					"RR": "Roraima",
					"SC": "Santa Catarina",
					"SP": "São Paulo",
					"SE": "Sergipe",
					"TO": "Tocantins"
				}, function (k, v) {
						estados.push({
								id: k,
								text: v
						});
				});

				$('#uf').editable({
						inputclass: 'form-control input-medium',
						source: estados,
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.cep').editable({
						type: 'text',
						name: 'cep',
						title: 'CEP',
						emptytext: "Vazio",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.email').editable({
						type: 'text',
						name: 'email',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
								if (validateEmail(value) == false) return '* Email inválido.';
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.telefonecomercial').editable({
						type: 'text',
						name: 'telefonecomercial',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.telefonecelular').editable({
						type: 'text',
						name: 'telefonecelular',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.obsPonto').editable({
						showbuttons: 'bottom',
						type: 'textarea',
						name: 'obsPonto',
						emptytext: 'Vazio',
						inputclass: 'form-control input-xlarge',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.obsOuvi').editable({
						showbuttons: 'bottom',
						type: 'textarea',
						name: 'obsOuvi',
						emptytext: 'Vazio',
						inputclass: 'form-control input-xlarge',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.docObs').editable({
						showbuttons: 'bottom',
						type: 'textarea',
						name: 'docObs',
						emptytext: 'Vazio',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.pendenciaIts').editable({
						showbuttons: true,
						type: 'select',
						name: 'pendenciaIts',
						emptytext: 'Vazio',
						inputclass: 'form-control input-xlarge',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.statusSolicitacao').editable({
						showbuttons: true,
						type: "select",
						name: "statusSolicitacao",
						inputclass: 'form-control',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 1,
										text: 'Solicitação'
								}, {
										value: 2,
										text: 'Aprovação'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataStatusSolicitacao').editable({
						type: 'date',
						name: 'dataStatusSolicitacao',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.statusCadastro').editable({
						showbuttons: true,
						type: "select",
						name: "statusCadastro",
						inputclass: 'form-control',
						validate: function (value) {
							if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
									value: 1,
									text: 'ATIVO'
								},{
									value: 0,
									text: 'INATIVO'
								}
						],
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				$('.dataStatusAtivacao').editable({
						type: 'date',
						name: 'dataStatusAtivacao',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataPendencia').editable({
						type: 'date',
						name: 'dataPendencia',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.statusAgenda').editable({
						showbuttons: true,
						type: "select",
						name: "statusAgenda",
						inputclass: 'form-control',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 1,
										text: 'EM ANDAMENTO'
								},{
										value: 2,
										text: 'PA EM FUNCIONAMENTO COM A AGENDA ABERTA'
								},{
										value: 3,
										text: 'PA EM FUNCIONAMENTO COM A AGENDA FECHADA'
								},{
										value: 6,
										text: 'CONTRATO CANCELADO'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.statusExibeSite').editable({
						showbuttons: true,
						type: "select",
						name: "statusExibeSite",
						inputclass: 'form-control',
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 1,
										text: 'ATIVO'
								},{
										value: 0,
										text: 'INATIVO'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.reciboEmissao').editable({
						showbuttons: true,
						type: "select",
						name: "reciboEmissao",
						title: 'Recibo Emissão Certificados',
						emptytext: "Vazio",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 0,
										text: 'NOTA FISCAL'
								},{
										value: 1,
										text: 'RECIBO DE PAGAMENTO'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.numContrato').editable({
						type: 'text',
						name: 'numContrato',
						title: 'Número do Contrato',
						inputclass: 'form-control input-xsmall',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataContrato').editable({
						type: 'date',
						name: 'dataContrato',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.nomeConsultor').editable({
						showbuttons: true,
						type: "select",
						name: "nomeConsultor",
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.nomeSupervisor').editable({
						showbuttons: true,
						type: "select",
						name: "nomeSupervisor",
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.nomeGerente').editable({
						showbuttons: true,
						type: "select",
						name: "nomeGerente",
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.nomeSuperintendente').editable({
						showbuttons: true,
						type: "select",
						name: "nomeSuperintendente",
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.geoLat').editable({
					type: 'text',
					name: 'geoLat',
					title: 'Latitude',
					emptytext: "Vazio",
					validate: function (value) {
						if ($.trim(value) == '') return 'Este campo é obrigatório';
					},
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				$('.geoLong').editable({
					type: 'text',
					name: 'geoLong',
					title: 'Longitude',
					emptytext: "Vazio",
					validate: function (value) {
						if ($.trim(value) == '') return 'Este campo é obrigatório';
					},
					success: function (response, newValue) {
						respostaBD(response);
					}
				});

				$('.senhaMaquina').editable({
						type: 'text',
						name: 'senhaMaquina',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.nomeGestor').editable({
						showbuttons: true,
						type: "select",
						name: "nomeGestor",
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.fabricante').editable({
						type: 'text',
						name: 'fabricante',
						title: 'Fabricante',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.modelo').editable({
						type: 'text',
						name: 'modelo',
						title: 'Modelo',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.numSerie').editable({
						type: 'text',
						name: 'numSerie',
						title: 'Número de Série',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.sistemaOperacional').editable({
						showbuttons: true,
						type: "select",
						name: "sistemaOperacional",
						title: 'Sistema Operacional',
						emptytext: "Vazio",
						inputclass: 'form-control',
						source: [{
										value: 'WINDOWS 10 PRO',
										text: 'WINDOWS 10 PRO'
								},{
										value: 'WINDOWS 8.1 PRO',
										text: 'WINDOWS 8.1 PRO'
								},{
										value: 'WINDOWS 8 PRO',
										text: 'WINDOWS 8 PRO'
								},{
										value: 'WINDOWS 7 PRO',
										text: 'WINDOWS 7 PRO'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.selo').editable({
						type: 'text',
						name: 'selo',
						title: 'Selo NF',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataSeloNF').editable({
						type: 'date',
						name: 'dataSeloNF',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						emptytext: 'Vazio',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.hostname').editable({
						type: 'text',
						name: 'hostname',
						title: 'Hostname',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.processador').editable({
						type: 'text',
						name: 'processador',
						title: 'Processador',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.memoria').editable({
						type: 'text',
						name: 'memoria',
						title: 'Memória',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.discoRigido').editable({
						type: 'text',
						name: 'discoRigido',
						title: 'Disco Rígido',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.monitor').editable({
						type: 'text',
						name: 'monitor',
						title: 'Monitor',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.placaVideo').editable({
						type: 'text',
						name: 'placaVideo',
						title: 'Placa de Vídeo',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.placaRede').editable({
						type: 'text',
						name: 'placaRede',
						title: 'Placa de Rede',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.ambiente').editable({
						showbuttons: true,
						type: "select",
						name: "ambiente",
						title: 'Ambiente',
						emptytext: "Vazio",
						inputclass: 'form-control',
						source: [{
										value: 'TREINAMENTO',
										text: 'TREINAMENTO'
								},{
										value: 'PRODUÇÃO',
										text: 'PRODUÇÃO'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.macAddress').editable({
						type: 'text',
						name: 'macAddress',
						title: 'Mac Address',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.conexaoInternet').editable({
						type: 'text',
						name: 'conexaoInternet',
						title: 'Conexão Internet',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.impressoraFabricante').editable({
						type: 'text',
						name: 'impressoraFabricante',
						title: 'Impressora Fabricante',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.impressoraModelo').editable({
						type: 'text',
						name: 'impressoraModelo',
						title: 'Impressora Modelo',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.impressoraNumSerie').editable({
						type: 'text',
						name: 'impressoraNumSerie',
						title: 'Impressora Núm. Série',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.softwareMaquina').editable({
						type: 'text',
						name: 'softwareMaquina',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.statusHabilitacao').editable({
						showbuttons: true,
						type: "select",
						name: "statusHabilitacao",
						inputclass: 'form-control',
						emptytext: "Vazio",
						validate: function (value) {
								if ($.trim(value) == '') return 'Este campo é obrigatório';
						},
						source: [{
										value: 1,
										text: 'Habilitada'
								}, {
										value: 2,
										text: 'Desabilitada'
								}, {
										value: 3,
										text: 'Em andamento'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataStatusHabilitacao').editable({
						type: 'date',
						name: 'dataStatusHabilitacao',
						format: 'yyyy-mm-dd',
						viewformat: 'dd/mm/yyyy',
						emptytext: 'Vazio',
						datepicker:{
							rtl: Metronic.isRTL(),
							orientation: 'left',
							autoclose: true
						},
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.obsMaquina').editable({
						showbuttons: 'bottom',
						type: 'textarea',
						name: 'obsMaquina',
						emptytext: 'Vazio',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.statusEstoque').editable({
						showbuttons: true,
						type: "select",
						name: "statusEstoque",
						title: 'Status Estoque',
						emptytext: "Vazio",
						inputclass: 'form-control',
						source: [{
										value: 0,
										text: 'Pendente'
								},{
										value: 1,
										text: 'OK'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.dataEstoque').editable({
					type: 'date',
					name: 'dataEstoque',
					format: 'yyyy-mm-dd',
					viewformat: 'dd/mm/yyyy',
					success: function(response, newValue) {
						respostaBD(response);
					}
				});

				$('.formaEnvio').editable({
						showbuttons: true,
						type: "select",
						name: "formaEnvio",
						title: 'Forma do Envio',
						emptytext: "Vazio",
						source: [{
										value: 'OK',
										text: 'OK'
								},{
										value: 'CORREIOS',
										text: 'CORREIOS'
								},{
										value: 'PORTADOR',
										text: 'PORTADOR'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.banco').editable({
						showbuttons: true,
						type: "select",
						name: "banco",
						title: 'Banco',
						emptytext: "Vazio",
						inputclass: 'form-control',
						source: [{
								value: '001',
								text: 'BANCO DO BRASIL'
							},{
								value: '341',
								text: 'ITAÚ'
							},{
								value: '033',
								text: 'SANTANDER'
							},{
								value: '237',
								text: 'BRADESCO'
							},{
								value: '745',
								text: 'CITIBANK'
							},{
								value: '104',
								text: 'CAIXA ECONÔMICA FEDERAL'
							},{
								value: '389',
								text: 'MERCANTIL DO BRASIL'
							},{
								value: '453',
								text: 'BANCO RURAL'
							},{
								value: '422',
								text: 'SAFRA'
							},{
								value: '399',
								text: 'HSBC BANK'
							},{
								value: '748',
								text: 'SICREDI'
							},{
								value: '756',
								text: 'SICOOB'
							}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				$('.agencia').editable({
						type: 'text',
						name: 'agencia',
						title: 'Agência',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				$('.numConta').editable({
						type: 'text',
						name: 'numConta',
						title: 'Núm. Conta',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				$('.operacao').editable({
						type: 'text',
						name: 'operacao',
						title: 'Operação',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				$('.titular').editable({
						type: 'text',
						name: 'titular',
						title: 'Nome do Titular',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});
				$('.docTitular').editable({
						type: 'text',
						name: 'docTitular',
						title: 'Documento do Titular',
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.boletoBancario').editable({
						showbuttons: true,
						type: "select",
						name: "boletoBancario",
						title: 'Boleto Bancário',
						emptytext: "Vazio",
						source: [{
										value: 0,
										text: 'NÃO'
								},{
										value: 1,
										text: 'SIM'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.faixaComissao').editable({
						showbuttons: true,
						type: "select",
						name: "faixaComissao",
						emptytext: "Vazio",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.arPai').editable({
						showbuttons: true,
						type: "select",
						name: "arPai",
						inputclass: 'form-control input-small',
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.cobrancaTED').editable({
						showbuttons: true,
						type: "select",
						name: "cobrancaTED",
						title: 'Cobrança TED',
						emptytext: "Vazio",
						inputclass: 'form-control',
						source: [{
										value: 1,
										text: 'Sim'
								},{
										value: 0,
										text: 'Não'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.cobrancaAgente').editable({
						showbuttons: true,
						type: "select",
						name: "cobrancaAgente",
						title: 'Cobrança por Agente (FGTS)',
						emptytext: "Vazio",
						inputclass: 'form-control',
						source: [{
										value: 1,
										text: 'Sim'
								},{
										value: 0,
										text: 'Não'
								}
						],
						success: function(response, newValue) {
								respostaBD(response);
						}
				});

				$('.descontoLink').editable({
						type: 'text',
						name: 'descontoLink',
						title: 'Desconto do Link',
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				$('.link').editable({
						type: 'textarea',
						name: 'link',
						title: 'Link',
						success: function(response, newValue) {
							respostaBD(response);
						}
				});

				$('.pontoVinculado').editable({
					source: 'assets/inc/lista-ponto.php',
					showbuttons: true,
					type: "select",
					name: "pontoVinculado",
					inputclass: 'form-control input-small',
					success: function(response, newValue) {
						respostaBD(response);
					}
				});

				$('.tipoPessoa').editable({
					showbuttons: true,
					type: "select",
					name: "tipoPessoa",
					emptytext: "Vazio",
					inputclass: 'form-control input-small',
					success: function(response, newValue) {
						respostaBD(response);
					}
				});

				$('.motOuvi').editable({
					showbuttons: true,
					type: "select",
					name: "motOuvi",
					emptytext: "Vazio",
					inputclass: 'form-control input-small',
					success: function(response, newValue) {
						respostaBD(response);
					}
				});

				$('.ouvidoriaResponsavel').editable({
					showbuttons: true,
					type: "select",
					name: "ouvidoriaResponsavel",
					emptytext: "Vazio",
					inputclass: 'form-control input-small',
					success: function(response, newValue) {
						respostaBD(response);
					}
				});
		}
		return {
				//main function to initiate the module
				init: function () {
						// init editable elements
						initEditables();
				}
		};
}();

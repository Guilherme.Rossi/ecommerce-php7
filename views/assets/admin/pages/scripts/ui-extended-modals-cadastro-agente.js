var UIExtendedModals = function () {

	return {
		//main function to initiate the module
		init: function () {

			// general settings
			$.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
				'<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
				'<div class="progress progress-striped active">' +
				'<div class="progress-bar" style="width: 100%;"></div>' +
				'</div>' +
				'</div>';

			$.fn.modalmanager.defaults.resize = true;

			var $modal = $('#cadastro-modal');

			$('.contato-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=contato', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.link-curso-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=link-curso', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.ctps-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=ctps', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.tipo-contrato-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=tipo-contrato', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.req-iti-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=req-iti', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.dados-bancarios-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=dados-bancarios', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.documentacao-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=documentacao', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.treinamento-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=treinamento', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.midia-treinamento-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=midia-treinamento', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.termos-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=termos', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.agente-certificado-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=agente-certificado', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.agente-contrato-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=agente-contrato', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.agente-aditivo-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro.php', 'idpessoa=' + idpessoa + '&tipo=agente-aditivo', function () {
						$modal.modal();
					});
				}, 1000);
			});


		}

	};

}();

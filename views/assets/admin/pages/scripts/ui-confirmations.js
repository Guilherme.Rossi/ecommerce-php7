var UIConfirmations = function () {

	var popoverAgente = function () {
		$('.popover-agente').on('confirmed.bs.confirmation', function () {
			$.ajax({
				url: 'assets/inc/form-editable-agente.php',
				type: 'POST',
				data: {
					'idpessoa': $(this).attr('data-idpessoa'),
					'tipo': $(this).attr('data-pk'),
					'idtipo': $(this).attr('data-idtipo'),
					'action': 'delete'
				},
				dataType: 'html',
				success: function(answer) {
					if(answer == 'ok'){
						toastr.success('Registro apagado com sucesso!', 'Cadastro Agente de Registro');
					};
				},
			});
		});

		$('.popover-agente').on('canceled.bs.confirmation', function () {

		});
	};

	var popoverPonto = function () {
		$('.popover-ponto').on('confirmed.bs.confirmation', function () {
			$.ajax({
				url: 'assets/inc/form-editable-ponto.php',
				type: 'POST',
				data: {
					'idpessoa': $(this).attr('data-idpessoa'),
					'tipo': $(this).attr('data-pk'),
					'idtipo': $(this).attr('data-idtipo'),
					'action': 'delete'
				},
				dataType: 'html',
				success: function(answer) {
					if(answer == 'ok'){
						toastr.success('Registro apagado com sucesso!', 'Cadastro ITS');
					};
				},
			});
		});

		$('.popover-ponto').on('canceled.bs.confirmation', function () {

		});
	};

	return {
		//main function to initiate the module
		init: function () {
			popoverAgente();
			popoverPonto();
		}
	};

}();

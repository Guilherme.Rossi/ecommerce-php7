var UIExtendedModals = function () {

	return {
		//main function to initiate the module
		init: function () {

			// general settings
			$.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
				'<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
				'<div class="progress progress-striped active">' +
				'<div class="progress-bar" style="width: 100%;"></div>' +
				'</div>' +
				'</div>';

			$.fn.modalmanager.defaults.resize = true;

			var $modal = $('#cadastro-modal');

			$('#contato-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=contato', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#solicitacao-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=solicitacao', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#agenda-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=agenda', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#agenda-exibicao-site-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=exibicao-site', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#recibo-emissao-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=recibo-emissao', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#estoque-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=estoque', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.software-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idmaquina = $(this).data('idmaquina');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idmaquina=' + idmaquina + '&tipo=software', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('.senha-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idmaquina = $(this).data('idmaquina');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idmaquina=' + idmaquina + '&tipo=senha', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#maquina-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=maquina', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#dados-bancarios-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=dados-bancarios', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#boleto-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=boleto-bancario', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#ouvidoria-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=ouvidoria', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#documento-novo-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=documento-novo', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#adicionar-motivo-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=adicionar-motivo', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#geo-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=tipo-geo', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#novo-responsavel-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=novo-responsavel', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#contato-responsavel-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=contato-responsavel', function () {
						$modal.modal();
					});
				}, 1000);
			});

			$('#doc-nova-filial-modal').on('click', function () {
				// create the backdrop and wait for next modal to be triggered
				$('body').modalmanager('loading');

				idpessoa = $(this).data('idpessoa');

				setTimeout(function () {
					$modal.load('assets/inc/ui_extended_modals_cadastro-ar.php', 'idpessoa=' + idpessoa + '&tipo=doc-nova-filial', function () {
						$modal.modal();
					});
				}, 1000);
			});

		}

	};

}();

var ChartsAmcharts = function() {

	var initChartContaPagarBarra = function() {
		var chart = AmCharts.makeChart("chartContaPagarBarra", {
			"type": "serial",
			"theme": "light",
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-dashboard.php?mesAno=dashboard&tipo=contaPagarBarra"
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-dashboard.php?mesAno=dashboard&tipo=contaPagarBarra"
			},
			"valueAxes": [{
				"gridColor": "#FFFFFF",
				"gridAlpha": 0.2,
				"dashLength": 0
			}],
			"gridAboveGraphs": true,
			"startDuration": 1,
			"graphs": [{
				"balloonText": "[[category]]: <b>[[value]]</b>",
				"fillAlphas": 0.8,
				"lineAlpha": 0.2,
				"type": "column",
				"valueField": "valor"
			}],
			"chartCursor": {
				"categoryBalloonEnabled": false,
				"cursorAlpha": 0,
				"zoomable": false
			},
			"categoryField": "tipo",
			"categoryAxis": {
				"gridPosition": "start",
				"gridAlpha": 0,
				"tickPosition": "start",
				"tickLength": 20
			},
			"export": {
				"enabled": true
			}
		});
		$('#chartContaPagarBarra').closest('.portlet').find('.fullscreen').click(function() {
			chart.invalidateSize();
		});
	}

	var initChartContaPagarPizza = function() {
		var pieChart = AmCharts.makeChart("chartContasPagarPizza", {
			"type": "pie",
			"theme": "light",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"titleField": "tipo",
			"valueField": "valor",
			"fontSize": 12,
			"allLabels": [],
			"balloon": {},
			"titles": [],
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-dashboard.php?mesAno=dashboard&tipo=contaPagarPizza"
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-dashboard.php?mesAno=dashboard&tipo=contaPagarPizza"
			},
			"outlineAlpha": 0.4,
			"depth3D": 20,
			"angle": 30,
			"colorField": "color"
		});
		$('#chartContasPagarPizza').closest('.portlet').find('.fullscreen').click(function() {
			chart.invalidateSize();
		});
	}

	var initChartEmissaoLinha = function() {
			var chart = AmCharts.makeChart("chartEmissaoLinha", {
					"type": "serial",
					"theme": "light",
					"language": "pt",

					"fontFamily": 'Open Sans',
					"color":    '#888888',

					"pathToImages": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/",

					"dataLoader": {
						//"url": "https://adm.certbank.com.br/assets/json/grafico-dashboard.php?mesAno=dashboard&tipo=emissaoDia"
						"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-dashboard.php?mesAno=dashboard&tipo=emissaoDia"
					},
					"balloon": {
							"cornerRadius": 6
					},
					"valueAxes": [{
						"gridColor": "#FFFFFF",
						"gridAlpha": 0.2,
						"dashLength": 0,
						"axisAlpha": 0
					}],
					"graphs": [{
							"bullet": "square",
							"bulletBorderAlpha": 1,
							"bulletBorderThickness": 1,
							"fillAlphas": 0.3,
							"fillColorsField": "lineColor",
							"legendValueText": "[[value]]",
							"lineColorField": "lineColor",
							"title": "quantidade",
							"valueField": "duration"
					}],
					"chartScrollbar": {},
					"chartCursor": {
							"categoryBalloonDateFormat": "DD/MM/YYYY",
							"cursorAlpha": 0,
							"zoomable": false
					},
					"dataDateFormat": "YYYY-MM-DD",
					"categoryField": "date",
					"categoryAxis": {
							"dateFormats": [{
									"period": "DD",
									"format": "DD"
							}, {
									"period": "WW",
									"format": "MMM DD"
							}, {
									"period": "MM",
									"format": "MMM"
							}, {
									"period": "YYYY",
									"format": "YYYY"
							}],
							"parseDates": true,
							"autoGridCount": false,
							"axisColor": "#555555",
							"gridAlpha": 0,
							"gridCount": 50
					}
			});

			$('#chartEmissaoLinha').closest('.portlet').find('.fullscreen').click(function() {
					chart.invalidateSize();
			});
	}

	var initChartEmissaoTipo = function() {
		var chart = AmCharts.makeChart("chart3dEmissaoTipo", {
			"theme": "light",
			"type": "serial",
			"startDuration": 2,
			"fontFamily": 'Open Sans',
			"color":    '#888',
			"dataLoader": {
				//"url": "https://adm.certbank.com.br/assets/json/grafico-dashboard.php?mesAno=dashboard&tipo=emissaoTipo"
				"url": "http://localhost/sites/CertBank/original/sistema/assets/json/grafico-dashboard.php?mesAno=dashboard&tipo=emissaoTipo"
			},
			"valueAxes": [{
				"position": "left",
				"axisAlpha": 250,
				"gridAlpha": 250,
				"title": "Qtd. Emissão por Tipo"
			}],
			"graphs": [{
				"balloonText": "[[category]]: <b>[[value]]</b>",
				"colorField": "color",
				"fillAlphas": 0.85,
				"lineAlpha": 0.1,
				"type": "column",
				"topRadius": 1,
				"valueField": "quantidade"
			}],
			"depth3D": 20,
			"angle": 15,
			"chartCursor": {
				"categoryBalloonEnabled": false,
				"cursorAlpha": 0,
				"zoomable": false
			},
			"categoryField": "nomeProduto",
			"categoryAxis": {
				"gridPosition": "start",
				"axisAlpha": 0,
				"gridAlpha": 0
			}
		});
		$('#chart3dEmissaoTipo').closest('.portlet').find('.fullscreen').click(function() {
			chart.invalidateSize();
		});
	}

	return {
		//main function to initiate the module
		init: function() {
			initChartContaPagarBarra();
			initChartEmissaoLinha();
			initChartContaPagarPizza();
			initChartEmissaoTipo();
		}
	};
}();

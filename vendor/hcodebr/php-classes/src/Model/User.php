<?php

namespace Hcode\Model;

use \Hcode\DB\Sql;
use \Hcode\Model;
use \Hcode\Mailer;

class User extends Model {

    const SESSION = "User";
    const SECRET = "informnovosistem";
    const SECRET_IV = "informnovosistem_IV";

    public static function login($login, $password){
        $sql = new Sql();
        $res = $sql->select("SELECT * FROM tb_users WHERE deslogin = :LOGIN", array(":LOGIN" => $login));

        if(count($res) === 0){
            throw new \Exception("Erro ao validar usúario ou senha.");
        }

        $data = $res[0];

        if(password_verify($password, $data['despassword']) === true ){
            $user = new User();

            $user->setData($data);

            $_SESSION[User::SESSION] = $user->getValues();

            return $user;

        } else {
            throw new \Exception("Erro ao validar usúario ou senha.");
        }
    }

    public static function verifyLogin($inadmin = true){
        if(
            !isset($_SESSION[User::SESSION])
            ||
            !$_SESSION[User::SESSION]
            ||
            !(int)$_SESSION[User::SESSION]['iduser'] > 0
            ||
            (bool)$_SESSION[User::SESSION]['inadmin'] !== $inadmin
        ){
            header("location: /admin/login");
            exit;
        }

    }

    public static function logout(){
        $_SESSION[User::SESSION] = NULL;
    }

    //////// CRUD /////

    public static function listAll(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM tb_users a INNER JOIN tb_persons b USING(idperson) ORDER BY b.desperson;");

    }

    public function createUser(){
        $sql = new Sql();
        $result = $sql->select("CALL sp_users_save(:pdesperson, :pdeslogin, :pdespassword, :pdesemail, :pnrphone, :pinadmin)", array(
        ":pdesperson" => $this->getdesperson(),
        ":pdeslogin" => $this->getdeslogin(),
        ":pdespassword" => $this->getdespassword(),
        ":pdesemail" => $this->getdesemail(),
        ":pnrphone" => $this->getnrphone(),
        ":pinadmin" => $this->getinadmin()));

        $this->setData($result[0]); 

    }

    public function get($iduser){
        $sql = new Sql();
        $result = $sql->select("SELECT * FROM tb_users a INNER JOIN tb_persons b USING(idperson) WHERE a.iduser = :IDUSER", array(":IDUSER" => $iduser));

        $this->setData($result[0]);
    }

    public function update(){
        $sql = new Sql();
        $result = $sql->select("CALL sp_usersupdate_save(:iduser, :pdesperson, :pdeslogin, :pdespassword, :pdesemail, :pnrphone, :pinadmin)", array(
            ":iduser" => $this->getiduser(),
            ":pdesperson" => $this->getdesperson(),
            ":pdeslogin" => $this->getdeslogin(),
            ":pdespassword" => $this->getdespassword(),
            ":pdesemail" => $this->getdesemail(),
            ":pnrphone" => $this->getnrphone(),
            ":pinadmin" => $this->getinadmin())
        );

        $this->setData($result[0]); 
    }

    public function delete(){
        $sql = new Sql();
        $sql->query("CALL sp_users_delete(:iduser)", array(":iduser" => $this->getiduser()));
        
    }
    ///////// FIM CRUD ///////////

    public static function getForgot($email){
        $sql = new Sql();
        $result = $sql->select("SELECT * FROM tb_persons a INNER JOIN tb_users b USING(idperson) WHERE a.desemail =  :EMAIL", array(":EMAIL"=> $email));
        
        if(count($result) === 0){
            throw new \Exception("Não foi possivel recuperar a senha.");
        } else {

            $data = $result[0];

            $recovery = $sql->select("CALL sp_userspasswordsrecoveries_create(:iduser, :ip)", array(
                        ":iduser"=> $data['iduser'],
                        ":ip" => $_SERVER["REMOTE_ADDR"]
                    ));

            if(count($recovery) === 0){
                throw new Exception("Não foi possivel recuperar a senha");
            } else {

                $dadosRecovery = $recovery[0];

                $code = openssl_encrypt($dadosRecovery['idrecovery'], 'AES-128-CBC', pack("a16", User::SECRET), 0, pack("a16", User::SECRET_IV));
                $result = base64_encode($code);

                $link = "http://www.siscertbank.com.br/admin/forgot/reset?code=$result";

                $mailer = new Mailer($data['desemail'], $data['desperson'], "Redefinir senha", "forgot", array(
                "name" => $data['desperson'],
                "link" => $link
                ));

                $mailer->send();

                return $data;
            }
        }

    }

    public static function validForgot($code){

        $result = base64_decode($code);
        $codeDec = openssl_decrypt($result, 'AES-128-CBC', pack("a16", User::SECRET), 0, pack("a16", User::SECRET_IV));

        $sql = new Sql();
        $result = $sql->select("SELECT * FROM tb_userspasswordsrecoveries a INNER JOIN tb_users b USING (iduser) INNER JOIN tb_persons c USING (idperson) WHERE a.idrecovery = :ID AND a.dtrecovery IS NULL AND DATE_ADD(a.dtregister, INTERVAL 1 HOUR) >= NOW();", array(":ID" => $codeDec));

        if (count($result) === 0){
            throw new \Exception("Não foi possivel recuperar a senha");
        } else {
            return $result[0];
        }
    }

    public static function setForgotUsed($idRecovery){
        $sql = new Sql();
        $sql->query("UPDATE tb_userspasswordsrecoveries SET dtrecovery = NOW() WHERE idrecovery = :ID", array(":ID"=>$idRecovery));

    }

    public function setPassword($password){
        $sql = new Sql();
        
        $sql->query("UPDATE tb_users SET despassword = :password WHERE iduser = :iduser", array(
            ":password" => $password,
            ":iduser" => $this->getiduser()
        ));
    }
}
?>